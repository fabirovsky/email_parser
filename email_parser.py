#!/usr/bin/env python3

from urllib import request
import re
import asyncio
import logging

import click
from lxml.html import fromstring, make_links_absolute

# Global variable for store emails
_emails = set()


def download_url(url):
    """
    A corountine to read the specified url and return a content.
    Returns set with url and content.
    """
    if not isinstance(url, str):
        raise ValueError("Value of url should be string")
    content = request.urlopen(url).read().decode('utf-8', errors='ignore')

    return content


def get_urls_with_base(string, base_url):
    """
    Returns all urls found in string, based on base_url. 
    For example, if base url is http://example.com/
    then it returns smth like [http://example.com/foo/, http://example.com/bar/]
    """
    doc = fromstring(string)
    doc.make_links_absolute(base_url)
    urls = [l[2] for l in doc.iterlinks() if l[0].tag == 'a']
    res = []
    for url in urls:
        if url.startswith(base_url) and url != base_url:
            res.append(url)
    return res


def get_emails(string):
    """
    Returns all email in string
    """
    re_email = r'[a-z0-9-]{1,30}@[a-z0-9-]{1,65}.[a-z]{1,}'
    emails = re.findall(re_email, string)
    return emails


def print_in_file(out_file):
    """
    Write message in file. 
    Returns function.
    """
    def write(message):
        out_file.write(message + '\n')
    return write


def print_in_sdout(message):
    """
    Write message in standart output.
    """
    click.echo(message)


async def _process_url(url, output):
    """
    Function for downloading, and parce url.
    It prints emails in specific output and returns found urls
    This function is corountine
    """
    content = download_url(url)

    global _emails
    emails = set(get_emails(content))
    for email in emails:
        if email not in _emails:
            output(email)
    _emails |= emails

    urls = get_urls_with_base(content, url)
    return urls


async def _main(urls, deep, output):
    """
    Main entry point for script.
    This function is corountine.
    """
    if output:
        output = print_in_file(output)
    else:
        output = print_in_sdout

    n_urls = []
    for _ in range(deep):
        corountines = [_process_url(url, output) for url in urls]
        completed, pending = await asyncio.wait(corountines)
        for item in completed:
            res = item.result()
            n_urls += res
        urls = n_urls


@click.command()
@click.option('--deep', '-d', default=1, help='How deep should it parse pages')
@click.option('--output', '-o', help='Output file', type=click.File('w'))
@click.argument('urls', nargs=-1, required=True)
def main(urls, deep, output):
    """
    Email parser for pages.
    Prints all found email on the specific web pages.
    """

    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(_main(list(urls), deep, output))
    finally:
        event_loop.close()


if __name__ == "__main__":
    main()
